<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>DMW</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="assets/css/main.css"  media="screen,projection"/>
</head>
<body>
    <?php include('assets/template/header.php'); ?>
    <?php include('assets/template/home.php'); ?>
    <?php include('assets/template/section-a.php'); ?>
    <?php include('assets/template/section-b.php'); ?>
    <?php include('assets/template/section-c.php'); ?>
    <?php include('assets/template/section-d.php'); ?>
    <?php include('assets/template/section-e.php'); ?>
    <?php include('assets/template/section-f.php'); ?>
    <?php include('assets/template/footer.php'); ?>
    
    <!--Javascript Start-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/materialize.min.js"></script>
    <script src="assets/js/init.js"></script>
    <script src="assets/js/main.js"></script>
    <!--Javascript End-->
</body>
</html>
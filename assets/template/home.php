<div id="index-banner" class="parallax-container">
  <div class="section no-pad-bot">
    <div class="container">
      <h1 class="header center teal-text text-lighten-2">DMW</h1>
    </div>
  </div>
  <div class="parallax"><img src="assets/images/background1.jpg"></div>
</div>

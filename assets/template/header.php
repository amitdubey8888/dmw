<nav role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="#" class="brand-logo">Logo</a>
    <ul class="right hide-on-med-and-down">
      <li><a href="#section-a">Navbar Link</a></li>
      <li><a href="#section-b">Navbar Link</a></li>
      <li><a href="#section-c">Navbar Link</a></li>
      <li><a href="#section-d">Navbar Link</a></li>
      <li><a href="#section-e">Navbar Link</a></li>
      <li><a href="#section-f">Navbar Link</a></li>
      <li><a href="#footer">Navbar Link</a></li>
    </ul>
    <ul id="nav-mobile" class="side-nav">
      <li><a href="#section-a">Navbar Link</a></li>
      <li><a href="#section-b">Navbar Link</a></li>
      <li><a href="#section-c">Navbar Link</a></li>
      <li><a href="#section-d">Navbar Link</a></li>
      <li><a href="#section-e">Navbar Link</a></li>
      <li><a href="#section-f">Navbar Link</a></li>
      <li><a href="#footer">Navbar Link</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
  </div>
</nav>

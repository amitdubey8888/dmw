<div class="parallax-container valign-wrapper" id="section-e">
  <div class="section no-pad-bot">
    <div class="container">
      <div class="row center">
        <h5 class="header col s12 light">A modern responsive front-end.</h5>
      </div>
    </div>
  </div>
  <div class="parallax"><img src="assets/images/background3.jpg" alt="Unsplashed background img 3"></div>
</div>
